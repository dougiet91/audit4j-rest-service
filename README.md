audit4j-rest-service
===

_This project is still under development. Notes will be added gradually as the project progresses..._



**Introduction**
---
This project was made to just try and demonstrate some of the simple workings of `Audit4J`.
There is a much more [detailed project](https://github.com/audit4j/audit4j-demo) officially from the development team behind the frame work

**The Tools**
---
This project was done using the following tools:

`Maven`,
`Java 8`,
`IntelliJ Idea` _(Although any IDE of your choice should work fine)_

**The Dependencies**
---
There are 4 main dependencies that you need to add to your pom file.
3 of them from `org.audit4j`:

```xml
        <dependency>
            <groupId>org.audit4j</groupId>
            <artifactId>audit4j-core</artifactId>
        </dependency>
        
        <dependency>
            <groupId>org.audit4j</groupId>
            <artifactId>audit4j-spring</artifactId>
        </dependency>
        
        <dependency>
            <groupId>org.audit4j</groupId>
            <artifactId>audit4j-db</artifactId>
        </dependency>
```
***as for the version of the dependencies, Its just wise to use 1 version.***


And since the framework uses aspects, 1 of them is from `org.aspectj`:
        <dependency>
            <groupId>org.aspectj</groupId>
            <artifactId>aspectjweaver</artifactId>
        </dependency>







To add security to your project you can try the implement **`Spring Security`** ([demonstrated here](https://spring.io/guides/gs/securing-web/))

we disabled [Cross Site Request Forgery (CSRF)](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF))





