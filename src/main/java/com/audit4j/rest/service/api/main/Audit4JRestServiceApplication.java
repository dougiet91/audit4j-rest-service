package com.audit4j.rest.service.api.main;

import com.audit4j.rest.service.api.config.ApiConfigMarkerInterface;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = {ApiConfigMarkerInterface.class})
public class Audit4JRestServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(Audit4JRestServiceApplication.class, args);
    }
}
