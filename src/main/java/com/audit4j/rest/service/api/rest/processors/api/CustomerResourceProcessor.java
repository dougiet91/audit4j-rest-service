package com.audit4j.rest.service.api.rest.processors.api;


import com.audit4j.rest.service.utils.messages.external.resquest.CustomerDto;
import com.audit4j.rest.service.utils.messages.external.response.CustomerResponseDto;

public interface CustomerResourceProcessor {
    CustomerResponseDto getAllCustomers();

    CustomerResponseDto getCustomer(CustomerDto customerDto, String language, String username);

    CustomerResponseDto saveCustomer(CustomerDto customerDto, String language, String username);

    CustomerResponseDto removeCustomer(CustomerDto customerDto, String language, String username);

    CustomerResponseDto editCustomer(CustomerDto customerDto, String language, String username);

    CustomerResponseDto createRandomCustomer(String language, String username);
}
