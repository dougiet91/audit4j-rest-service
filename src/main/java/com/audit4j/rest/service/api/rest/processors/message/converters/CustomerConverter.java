package com.audit4j.rest.service.api.rest.processors.message.converters;

import com.audit4j.rest.service.domain.Customer;
import com.audit4j.rest.service.utils.messages.external.response.CustomerResponseDto;
import com.audit4j.rest.service.utils.messages.external.resquest.CustomerDto;
import com.audit4j.rest.service.utils.messages.internal.response.CustomerResponse;
import com.audit4j.rest.service.utils.messages.internal.response.StandardResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created By Dougie T Muringani : 24/5/2019
 */
public class CustomerConverter {

    public static CustomerDto convertCustomer(Customer customer){
        if(customer == null){
            return null;
        }
        CustomerDto dto = new CustomerDto();

        dto.setId(customer.getId());
        dto.setDateOfBirth(customer.getDob());
        dto.setFirstName(customer.getFirstName());
        dto.setLastName(customer.getLastName());
        dto.setMsisdn(customer.getMsisdn());

        return dto;
    }

    public static Customer convertCustomer(CustomerDto dto){
        if(dto == null){
            return null;
        }
        Customer customer = new Customer();

        customer.setId(dto.getId());
        customer.setDob(dto.getDateOfBirth());
        customer.setFirstName(dto.getFirstName());
        customer.setLastName(dto.getLastName());
        customer.setMsisdn(dto.getMsisdn());

        return customer;
    }

    public static List<Customer> convertCustomerDtoList(List<CustomerDto> dtos){
        if(dtos == null){
            return Collections.emptyList();
        }
        return dtos.stream().map(CustomerConverter::convertCustomer).collect(Collectors.toList());
    }

    public static List<CustomerDto> convertCustomerList(List<Customer> customers){
        if(customers == null){
            return Collections.emptyList();
        }
        return customers.stream().map(CustomerConverter::convertCustomer).collect(Collectors.toList());
    }

    public static CustomerResponseDto convertCustomerResponse(CustomerResponse response){
        CustomerResponseDto responseDto = new CustomerResponseDto();

        responseDto.mapStandardResponse(response);
        responseDto.setCustomer(convertCustomer(response.getCustomer()));
        responseDto.setCustomers(convertCustomerList(response.getCustomerList()));

        return responseDto;
    }


}
