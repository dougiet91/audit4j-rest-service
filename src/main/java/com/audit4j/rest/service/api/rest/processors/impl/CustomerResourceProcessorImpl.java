package com.audit4j.rest.service.api.rest.processors.impl;

import com.audit4j.rest.service.api.rest.processors.api.CustomerResourceProcessor;
import com.audit4j.rest.service.business.services.logic.api.CustomerService;
import com.audit4j.rest.service.domain.BlobObject;
import com.audit4j.rest.service.utils.constants.SystemConstants;
import com.audit4j.rest.service.utils.messages.external.response.CustomerResponseDto;
import com.audit4j.rest.service.utils.messages.external.resquest.CustomerDto;

import java.util.Locale;

import static com.audit4j.rest.service.api.rest.processors.message.converters.CustomerConverter.convertCustomer;
import static com.audit4j.rest.service.api.rest.processors.message.converters.CustomerConverter.convertCustomerResponse;
import static com.audit4j.rest.service.business.services.ObjectGeneratorUtil.generateRandomBlob;

public class CustomerResourceProcessorImpl implements CustomerResourceProcessor {

    private CustomerService customerService;

    public CustomerResourceProcessorImpl(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public CustomerResponseDto getAllCustomers() {
        return convertCustomerResponse(customerService.findAll(new Locale(SystemConstants.LOCALE_LANGUAGE)));
    }

    @Override
    public CustomerResponseDto getCustomer(CustomerDto customerDto, String language, String username) {
        return convertCustomerResponse(customerService.findCustomerById(customerDto.getId(), new Locale(language)));
    }

    @Override
    public CustomerResponseDto saveCustomer(CustomerDto customerDto, String language, String username) {
        return convertCustomerResponse(customerService.save(convertCustomer(customerDto), new Locale(language), username));
    }

    @Override
    public CustomerResponseDto removeCustomer(CustomerDto customerDto, String language, String username) {
        return convertCustomerResponse(customerService.remove(customerDto.getId(), new Locale(language), username));
    }

    @Override
    public CustomerResponseDto editCustomer(CustomerDto customerDto, String language, String username) {
        return saveCustomer(customerDto, language, username);
    }

    @Override
    public CustomerResponseDto createRandomCustomer(String language, String username) {
        BlobObject blob = generateRandomBlob();
        CustomerDto customerDto = new CustomerDto();

        customerDto.setFirstName(blob.getFirstName());
        customerDto.setLastName(blob.getLastName());
        customerDto.setMsisdn(Long.valueOf(blob.getMsisdn()));
        customerDto.setDateOfBirth(blob.getBlobDate());

        return saveCustomer(customerDto, language, username);
    }
}
