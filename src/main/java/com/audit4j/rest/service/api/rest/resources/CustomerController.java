package com.audit4j.rest.service.api.rest.resources;

import com.audit4j.rest.service.api.rest.processors.api.CustomerResourceProcessor;
import com.audit4j.rest.service.utils.constants.SystemConstants;
import com.audit4j.rest.service.utils.messages.external.response.CustomerResponseDto;
import com.audit4j.rest.service.utils.messages.external.resquest.CustomerDto;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import static com.audit4j.rest.service.utils.constants.SystemConstants.DEFAULT_USER;

@Slf4j
@RestController
@RequestMapping("/customers")
public class CustomerController {

    private CustomerResourceProcessor processor;

    public CustomerController(CustomerResourceProcessor customerResourceProcessor) {
        this.processor = customerResourceProcessor;
    }

    @ApiOperation(value = "save Customer - Save customer. New or Edited",
            response = CustomerResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request Successfully submitted"),
            @ApiResponse(code = 401, message = "You are not authorized to make request"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PostMapping(value = "generate", produces = {MediaType.APPLICATION_JSON_VALUE})
    public CustomerResponseDto createRandomCustomer(@RequestHeader(value = SystemConstants.LOCALE_LANGUAGE,
                                                    defaultValue = SystemConstants.DEFAULT_LOCALE) final String language) {
        log.info("Received Request to create random customer");
        return processor.createRandomCustomer(language, DEFAULT_USER);
    }

    @ApiOperation(value = "save Customer - Save customer. New or Edited",
            response = CustomerResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request Successfully submitted"),
            @ApiResponse(code = 401, message = "You are not authorized to make request"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PostMapping(value = "create", produces = {MediaType.APPLICATION_JSON_VALUE})
    public CustomerResponseDto saveCustomer(@RequestBody final CustomerDto request,
                                            @RequestHeader(value = SystemConstants.LOCALE_LANGUAGE,
                                                    defaultValue = SystemConstants.DEFAULT_LOCALE) final String language) {
        return processor.saveCustomer(request, language, DEFAULT_USER);
    }


    @ApiOperation(value = "Find Customers - all",
            response = CustomerResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request Successfully submitted"),
            @ApiResponse(code = 401, message = "You are not authorized to make request"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping(value = "all", produces = {MediaType.APPLICATION_JSON_VALUE})
    CustomerResponseDto getAllCustomers() {
        return processor.getAllCustomers();
    }


    @ApiOperation(value = "Find Customer - One, By Id",
            response = CustomerResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request Successfully submitted"),
            @ApiResponse(code = 401, message = "You are not authorized to make request"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @GetMapping(value = "customer/id/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    CustomerResponseDto getCustomer(@PathVariable(name = "id") final Long id,
                                    @RequestHeader(value = SystemConstants.LOCALE_LANGUAGE,
                                            defaultValue = SystemConstants.DEFAULT_LOCALE) final String language) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(id);
        return processor.getCustomer(customerDto, language, DEFAULT_USER);
    }


    @ApiOperation(value = "Delete Customer - One, By Id",
            response = CustomerResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request Successfully submitted"),
            @ApiResponse(code = 401, message = "You are not authorized to make request"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @DeleteMapping(value = "delete/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    CustomerResponseDto removeCustomer(@PathVariable(name = "id") final Long id,
                                       @RequestHeader(value = SystemConstants.LOCALE_LANGUAGE,
                                               defaultValue = SystemConstants.DEFAULT_LOCALE) final String language) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(id);
        return processor.removeCustomer(customerDto, language, DEFAULT_USER);
    }


    @ApiOperation(value = "Edit Customer - Save customer. Edited",
            response = CustomerResponseDto.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Request Successfully submitted"),
            @ApiResponse(code = 401, message = "You are not authorized to make request"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @PostMapping(value = "edit", produces = {MediaType.APPLICATION_JSON_VALUE})
    CustomerResponseDto editCustomer(@RequestBody final CustomerDto customerDto,
                                     @RequestHeader(value = SystemConstants.LOCALE_LANGUAGE,
                                             defaultValue = SystemConstants.DEFAULT_LOCALE) final String language) {
        return processor.editCustomer(customerDto, language, DEFAULT_USER);
    }
}
