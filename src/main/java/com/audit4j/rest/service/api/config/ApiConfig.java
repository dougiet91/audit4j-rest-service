package com.audit4j.rest.service.api.config;


import com.audit4j.rest.service.api.rest.processors.api.CustomerResourceProcessor;
import com.audit4j.rest.service.api.rest.resources.CustomerController;
import com.audit4j.rest.service.business.config.RootConfig;
import com.audit4j.rest.service.business.services.logic.api.CustomerService;
import com.audit4j.rest.service.utils.config.SwaggerConfig;
import com.audit4j.rest.service.api.rest.processors.impl.CustomerResourceProcessorImpl;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan(basePackages = "com.audit4j.rest.service.api.rest.resources")
@Import({SwaggerConfig.class, AspectConfig.class, RootConfig.class})

public class ApiConfig {
//    @Bean
//    CustomerController customerController(CustomerResourceProcessor customerResourceProcessor){
//        return new CustomerController(customerResourceProcessor);
//    }

    @Bean
    CustomerResourceProcessor customerResourceProcessor(CustomerService customerService) {
        return new CustomerResourceProcessorImpl(customerService);
    }

}
