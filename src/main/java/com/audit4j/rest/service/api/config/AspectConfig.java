package com.audit4j.rest.service.api.config;

import com.audit4j.rest.service.api.aspects.AspectMarkerInterface;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

/**
 * Created By Dougie T Muringani : 17/9/2018
 */
@Configuration
@EnableAspectJAutoProxy
@ComponentScan(basePackageClasses = {AspectMarkerInterface.class})
public class AspectConfig {
}
