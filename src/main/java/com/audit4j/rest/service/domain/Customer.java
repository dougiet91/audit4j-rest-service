package com.audit4j.rest.service.domain;

/**
 * Created By Dougie T Muringani : 24/5/2019
 */
import com.audit4j.rest.service.utils.enums.EntityStatus;
import com.audit4j.rest.service.utils.keygen.GenerateKey;
import lombok.Data;
import org.audit4j.core.annotation.AuditField;

import javax.persistence.*;
import java.util.*;

@Data
@Entity
@Table(name = "customer")
public class Customer {

    @AuditField(field = "payment id")
    @Id
    private Long id;

    @AuditField(field = "first_name")
    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "dob")
    private Date dob;

    @Column(name = "msisdn")
    private Long msisdn;



    @Enumerated(EnumType.STRING)
    @Column(length = 30, nullable = false)
    private EntityStatus status;



    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "date_last_updated")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateLastUpdated;

    @PrePersist
    public void init() {
        if (id == null) {
            id = GenerateKey.generateEntityId();
        }

        if (status == null) {
            status = EntityStatus.ACTIVE;
        }

        if (creationDate == null) {
            creationDate = new Date();
        }
    }

    @PreUpdate
    public void reload() {
        dateLastUpdated = new Date();
    }
}
