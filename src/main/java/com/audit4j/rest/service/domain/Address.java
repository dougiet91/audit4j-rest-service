package com.audit4j.rest.service.domain;

import com.audit4j.rest.service.utils.enums.EntityStatus;
import com.audit4j.rest.service.utils.keygen.GenerateKey;

import javax.persistence.*;
import java.util.Date;

/**
 * Created By Dougie T Muringani : 24/1/2019
 */
@Entity
@Table(name="address")
public class Address {
    @Id
    private Long id;


    @Column(name = "street_number", length = 15)
    private String streetNumber;

    @Column(name = "street_name", length = 30)
    private String streetName;

    @Column(name = "po_box_bag", length = 20)
    private String box;

    @Column(name = "p_bag", length = 20)
    private String bag;

    @Column(name = "suburb", length = 40)
    private String suburb;

    @Column(name = "district", length = 30)
    private String district;

    @Column(name = "town", length = 40)
    private String town;

    @Column(name = "province", length = 30)
    private String province;

    @Column(name = "state", length = 30)
    private String state;

    @Column(name = "zip_code")
    private Long zipCode;

    @Column(name = "postal_code")
    private Long postalCode;

    @Column(name = "country", length = 30)
    private String country;


    @Column(name = "entity_status", length = 30, nullable = false)
    @Enumerated(EnumType.STRING)
    private EntityStatus entityStatus;

    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    @Column(name = "modification_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date modificationDate;

    @PrePersist
    public void init() {
        if(id == null) {
            id = GenerateKey.generateEntityId();
        }

        if(entityStatus == null) {
            entityStatus = EntityStatus.ACTIVE;
        }

        if(creationDate == null) {
            creationDate = new Date();
        }
        modificationDate = new Date();
    }

    @PreUpdate
    public void reload() {
        modificationDate = new Date();
    }




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getZipCode() {
        return zipCode;
    }

    public void setZipCode(Long zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Long getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Long postalCode) {
        this.postalCode = postalCode;
    }

    public String getBag() {
        return bag;
    }

    public void setBag(String bag) {
        this.bag = bag;
    }

    public String getBox() {
        return box;
    }

    public void setBox(String box) {
        this.box = box;
    }
}
