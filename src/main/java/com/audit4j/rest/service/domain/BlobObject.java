package com.audit4j.rest.service.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "blob_object")
@NamedQueries({
		@NamedQuery(name = "BlobRepository.findBlobById",
				query = "select b from BlobObject b where b.id = :id"),
})

public class BlobObject {

    @Id
    private String id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "gender")
    private String gender;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "car_model")
    private String carModel;

    @Column(name = "bitcoin_address")
    private String bitcoinAddress;

    @Column(name = "blob_boolean")
    private String blobBoolean;

    @Column(name = "blob_year")
    private long blobYear;

    @Column(name = "company_name")
    private String companyName;

    @Column(name = "country_code")
    private String countryCode;

    @Column(name = "credit_card_number")
    private double creditCardNumber;

    @Column(name = "country")
    private String country;

    @Column(name = "colour")
    private String colour;

    @Column(name = "credit_card_type")
    private String creditCardType;

    @Column(name = "currency")
    private String currency;

    @Column(name = "currency_code")
    private String currencyCode;

    @Column(name = "blob_date")
    private Date blobDate;

    @Column(name = "department")
    private String department;

    @Column(name = "digit_sequence")
    private String digitSequence;

    @Column(name = "domain_name")
    private String domainName;

    @Column(name = "duns_number")
    private String dunsNumber;

    @Column(name = "employer_identification_number")
    private String employerIdentificationNumber;

    @Column(name = "fake_company_name")
    private String fakeCompanyName;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "frequency")
    private String frequency;

    @Column(name = "gender_abbrev")
    private String genderAbbrev;

    @Column(name = "guid")
    private String guid;

    @Column(name = "hex_colour")
    private String hexColour;

    @Column(name = "isbn")
    private String isbn;

    @Column(name = "job_title")
    private String jobTitle;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "skill")
    private String skill;

    @Column(name = "longitude")
    private double longitude;

    @Column(name = "mac_address")
    private String macAddress;

    @Column(name = "mime_type")
    private String mimeType;

    @Column(name = "money")
    private String money;

    @Column(name = "movie_genre")
    private String movieGenre;

    @Column(name = "movie_title")
    private String movieTitle;

    @Column(name = "phonetic")
    private String phonetic;

    @Column(name = "blob_password")
    private String blobPassword;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "ethnicity")
    private String ethnicity;

    @Column(name = "size")
    private String size;

    @Column(name = "street_number")
    private long streetNumber;

    @Column(name = "street_name")
    private String streetName;

    @Column(name = "street_suffix")
    private String streetSuffix;

    @Column(name = "street_address")
    private String streetAddress;

    @Column(name = "blob_time")
    private String blobTime;

    @Column(name = "time_zone")
    private String timeZone;

    @Column(name = "title")
    private String title;

    @Column(name = "user_agent")
    private String userAgent;

    @Column(name = "url")
    private String url;

    @Column(name = "username")
    private String username;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }


    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }


    public String getBitcoinAddress() {
        return bitcoinAddress;
    }

    public void setBitcoinAddress(String bitcoinAddress) {
        this.bitcoinAddress = bitcoinAddress;
    }


    public String getBlobBoolean() {
        return blobBoolean;
    }

    public void setBlobBoolean(String blobBoolean) {
        this.blobBoolean = blobBoolean;
    }


    public long getBlobYear() {
        return blobYear;
    }

    public void setBlobYear(long blobYear) {
        this.blobYear = blobYear;
    }


    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    public double getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(double creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }


    public String getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }


    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }


    public Date getBlobDate() {
        return blobDate;
    }

    public void setBlobDate(Date blobDate) {
        this.blobDate = blobDate;
    }


    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }


    public String getDigitSequence() {
        return digitSequence;
    }

    public void setDigitSequence(String digitSequence) {
        this.digitSequence = digitSequence;
    }


    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }


    public String getDunsNumber() {
        return dunsNumber;
    }

    public void setDunsNumber(String dunsNumber) {
        this.dunsNumber = dunsNumber;
    }


    public String getEmployerIdentificationNumber() {
        return employerIdentificationNumber;
    }

    public void setEmployerIdentificationNumber(String employerIdentificationNumber) {
        this.employerIdentificationNumber = employerIdentificationNumber;
    }


    public String getFakeCompanyName() {
        return fakeCompanyName;
    }

    public void setFakeCompanyName(String fakeCompanyName) {
        this.fakeCompanyName = fakeCompanyName;
    }


    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }


    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }


    public String getGenderAbbrev() {
        return genderAbbrev;
    }

    public void setGenderAbbrev(String genderAbbrev) {
        this.genderAbbrev = genderAbbrev;
    }


    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }


    public String getHexColour() {
        return hexColour;
    }

    public void setHexColour(String hexColour) {
        this.hexColour = hexColour;
    }


    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }


    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }


    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }


    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }


    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }


    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }


    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }


    public String getMovieGenre() {
        return movieGenre;
    }

    public void setMovieGenre(String movieGenre) {
        this.movieGenre = movieGenre;
    }


    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }


    public String getPhonetic() {
        return phonetic;
    }

    public void setPhonetic(String phonetic) {
        this.phonetic = phonetic;
    }


    public String getBlobPassword() {
        return blobPassword;
    }

    public void setBlobPassword(String blobPassword) {
        this.blobPassword = blobPassword;
    }


    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }


    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }


    public long getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(long streetNumber) {
        this.streetNumber = streetNumber;
    }


    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }


    public String getStreetSuffix() {
        return streetSuffix;
    }

    public void setStreetSuffix(String streetSuffix) {
        this.streetSuffix = streetSuffix;
    }


    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }


    public String getBlobTime() {
        return blobTime;
    }

    public void setBlobTime(String blobTime) {
        this.blobTime = blobTime;
    }


    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "BlobObject{" +
                "  id='" + id + '\'' +
                ",\n  firstName='" + firstName + '\'' +
                ",\n  lastName='" + lastName + '\'' +
                ",\n  fullName='" + fullName + '\'' +
                ",\n  email='" + email + '\'' +
                ",\n  gender='" + gender + '\'' +
                ",\n  ipAddress='" + ipAddress + '\'' +
                ",\n  carModel='" + carModel + '\'' +
                ",\n  bitcoinAddress='" + bitcoinAddress + '\'' +
                ",\n  blobBoolean='" + blobBoolean + '\'' +
                ",\n  blobYear=" + blobYear +
                ",\n  companyName='" + companyName + '\'' +
                ",\n  countryCode='" + countryCode + '\'' +
                ",\n  creditCardNumber=" + creditCardNumber +
                ",\n  country='" + country + '\'' +
                ",\n  colour='" + colour + '\'' +
                ",\n  creditCardType='" + creditCardType + '\'' +
                ",\n  currency='" + currency + '\'' +
                ",\n  currencyCode='" + currencyCode + '\'' +
                ",\n  blobDate=" + blobDate +
                ",\n  department='" + department + '\'' +
                ",\n  digitSequence='" + digitSequence + '\'' +
                ",\n  domainName='" + domainName + '\'' +
                ",\n  dunsNumber='" + dunsNumber + '\'' +
                ",\n  employerIdentificationNumber='" + employerIdentificationNumber + '\'' +
                ",\n  fakeCompanyName='" + fakeCompanyName + '\'' +
                ",\n  fileName='" + fileName + '\'' +
                ",\n  frequency='" + frequency + '\'' +
                ",\n  genderAbbrev='" + genderAbbrev + '\'' +
                ",\n  guid='" + guid + '\'' +
                ",\n  hexColour='" + hexColour + '\'' +
                ",\n  isbn='" + isbn + '\'' +
                ",\n  jobTitle='" + jobTitle + '\'' +
                ",\n  latitude=" + latitude +
                ",\n  skill='" + skill + '\'' +
                ",\n  longitude=" + longitude +
                ",\n  macAddress='" + macAddress + '\'' +
                ",\n  mimeType='" + mimeType + '\'' +
                ",\n  money='" + money + '\'' +
                ",\n  movieGenre='" + movieGenre + '\'' +
                ",\n  movieTitle='" + movieTitle + '\'' +
                ",\n  phonetic='" + phonetic + '\'' +
                ",\n  blobPassword='" + blobPassword + '\'' +
                ",\n  msisdn='" + msisdn + '\'' +
                ",\n  ethnicity='" + ethnicity + '\'' +
                ",\n  size='" + size + '\'' +
                ",\n  streetNumber=" + streetNumber +
                ",\n  streetName='" + streetName + '\'' +
                ",\n  streetSuffix='" + streetSuffix + '\'' +
                ",\n  streetAddress='" + streetAddress + '\'' +
                ",\n  blobTime='" + blobTime + '\'' +
                ",\n  timeZone='" + timeZone + '\'' +
                ",\n  title='" + title + '\'' +
                ",\n  userAgent='" + userAgent + '\'' +
                ",\n  url='" + url + '\'' +
                ",\n  username='" + username + '\'' +
                '}';
    }
}
