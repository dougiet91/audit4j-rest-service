package com.audit4j.rest.service.utils.keygen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenerateKey {

    private static final Logger log = LoggerFactory.getLogger(GenerateKey.class);

    public static long generateEntityId() {

        log.info("Generating new Entity Id");
        long l = System.currentTimeMillis();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            builder.append((int) (Math.random() * 10));
        }
        int randomNumber = (int) (Math.random() * 1000);

        String entityId = l + builder.toString() + randomNumber;
        try {
            l = Long.parseLong(entityId);
        } catch (Exception e) {
            return Math.abs(l);
        }
        return Math.abs(l);
    }

    public static void main(String... strings) {
        log.info(">> Generated Key : {}", + generateEntityId());
        log.info("{}", Math.random());
        log.info("{}", Long.MAX_VALUE);
    }


}
