package com.audit4j.rest.service.utils.messages.internal.response;

import com.audit4j.rest.service.domain.Customer;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

import static com.audit4j.rest.service.utils.ListUtils.printList;

@Data
public class CustomerResponse extends StandardResponse{
    @ApiModelProperty(notes = "customer", example = "customer{customer-details}", required = true)
    @JsonProperty("customer")
    private Customer customer;

    @ApiModelProperty(notes = "customer List", example = "customer[{customer-details},{customer-details},{customer-details}]", required = true)
    @JsonProperty("customerList")
    private List<Customer> customerList;

    @Override
    public String toString() {
        return super.standardToString() + "CustomerResponse{" +
                "     customer =" + customer +
                ",\n    customerList =" + printList(customerList) +
                '}';
    }
}
