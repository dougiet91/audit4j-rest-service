package com.audit4j.rest.service.utils.enums;

public enum EntityStatus {
    NEW, DRAFT, SUSPENDED, ACTIVE,DEACTIVATED,DELETED

}