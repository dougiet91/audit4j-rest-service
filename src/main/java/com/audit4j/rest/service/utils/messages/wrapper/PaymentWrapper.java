package com.audit4j.rest.service.utils.messages.wrapper;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * Created By Dougie T Muringani : 25/10/2018
 */
public class PaymentWrapper {

    @ApiModelProperty(notes = "id", example = "12345", required = true)
    @JsonProperty("id")
    private Long id;

    @ApiModelProperty(notes = "customerAccount", example = "12345", required = true)
    @JsonProperty("customerAccount")
    private String customerAccount;

    @ApiModelProperty(notes = "documentNumber", example = "12345", required = true)
    @JsonProperty("documentNumber")
    private String documentNumber;

    @ApiModelProperty(notes = "branchId", example = "12345", required = true)
    @JsonProperty("branchId")
    private String branchId;

    @ApiModelProperty(notes = "cashierId", example = "EU4", required = true)
    @JsonProperty("cashierId")
    private String cashierId;

    @ApiModelProperty(notes = "transactionType", example = "exampleData", required = true)
    @JsonProperty("transactionType")
    private String transactionType;

    @ApiModelProperty(notes = "billingStatus", example = "POSTED, PENDING_POSTING", required = true)
    @JsonProperty("billingStatus")
    private String billingStatus;

    @ApiModelProperty(notes = "erpStatus", example = "POSTED, PENDING_POSTING", required = true)
    @JsonProperty("erpStatus")
    private String erpStatus;

    @ApiModelProperty(notes = "offlineReceiptNumber", example = "12345", required = true)
    @JsonProperty("offlineReceiptNumber")
    private String offlineReceiptNumber;


    @ApiModelProperty(notes = "status", example = "POSTED, PENDING_POSTING", required = true)
    @JsonProperty("status")
    private String status;


    @ApiModelProperty(notes = "creationDate", example = "2019-02-16T00:00:00", required = true)
    @JsonProperty("creationDate")
    private Date creationDate;

    @ApiModelProperty(notes = "startDate", example = "2019-02-16T00:00:00", required = true)
    @JsonProperty("startDate")
    private Date startDate;

    @ApiModelProperty(notes = "endDate", example = "2019-02-16T00:00:00", required = true)
    @JsonProperty("endDate")
    private Date endDate;

    public String getBillingStatus() {
        return billingStatus;
    }

    public void setBillingStatus(String billingStatus) {
        this.billingStatus = billingStatus;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getCashierId() {
        return cashierId;
    }

    public void setCashierId(String cashierId) {
        this.cashierId = cashierId;
    }

    public String getCustomerAccount() {
        return customerAccount;
    }

    public void setCustomerAccount(String customerAccount) {
        this.customerAccount = customerAccount;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getErpStatus() {
        return erpStatus;
    }

    public void setErpStatus(String erpStatus) {
        this.erpStatus = erpStatus;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getOfflineReceiptNumber() {
        return offlineReceiptNumber;
    }

    public void setOfflineReceiptNumber(String offlineReceiptNumber) {
        this.offlineReceiptNumber = offlineReceiptNumber;
    }

    @Override
    public String toString() {
        return "PaymentWrapper{" +
                "  id=" + id +
                ",\n  customerAccount='" + customerAccount + '\'' +
                ",\n  documentNumber='" + documentNumber + '\'' +
                ",\n  branchId='" + branchId + '\'' +
                ",\n  cashierId='" + cashierId + '\'' +
                ",\n  transactionType='" + transactionType + '\'' +
                ",\n  billingStatus='" + billingStatus + '\'' +
                ",\n  erpStatus='" + erpStatus + '\'' +
                ",\n  offlineReceiptNumber='" + offlineReceiptNumber + '\'' +
                ",\n  status='" + status + '\'' +
                ",\n  creationDate=" + creationDate +
                ",\n  startDate=" + startDate +
                ",\n  endDate=" + endDate +
                '}';
    }
}
