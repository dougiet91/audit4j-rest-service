package com.audit4j.rest.service.utils.messages.external.response;

import com.audit4j.rest.service.utils.messages.internal.response.StandardResponse;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.audit4j.rest.service.utils.messages.external.resquest.CustomerDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class CustomerResponseDto extends StandardResponse {
    @ApiModelProperty(notes = "List Of Customers", required = true)
    @JsonProperty("customers")
    List<CustomerDto> customers;

    @ApiModelProperty(notes = "Customer", required = true)
    @JsonProperty("customer")
    CustomerDto customer;
}
