package com.audit4j.rest.service.utils.messages.internal.request.wrappers;

import lombok.Data;

/**
 * Created By Dougie T Muringani : 14/12/2018
 */
@Data
public class RoleWrapper {
    private Long id;
    private String name;
    private String description;
}
