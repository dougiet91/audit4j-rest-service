package com.audit4j.rest.service.utils.enums;

/**
 * Created By Dougie T Muringani : 14/12/2018
 */
public enum CrudOperation {
    CREATE,READ,UPDATE,DELETE
}
