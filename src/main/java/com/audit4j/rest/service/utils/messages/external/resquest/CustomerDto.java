package com.audit4j.rest.service.utils.messages.external.resquest;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class CustomerDto {
    @ApiModelProperty(notes = "Customer Id", example = "1234", required = true)
    @JsonProperty("id")
    private Long id;
    @ApiModelProperty(notes = "Customer's First Name", example = "Jane", required = true)
    @JsonProperty("first_name")
    private String firstName;
    @ApiModelProperty(notes = "Customer's Last Name (Surname)", example = "Doe", required = true)
    @JsonProperty("last_name")
    private String lastName;
    @ApiModelProperty(notes = "Customer's Date of Birth", example = "10/31/1991", required = true)
    @JsonProperty("date_of_birth")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy")
    private Date dateOfBirth;
    @ApiModelProperty(notes = "Customer's Preferred Mobile Number", example = "784000000", required = true)
    @JsonProperty("mobile_number")
    private Long msisdn;
}
