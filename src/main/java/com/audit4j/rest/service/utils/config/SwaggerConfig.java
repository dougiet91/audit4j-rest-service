package com.audit4j.rest.service.utils.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;


@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Autowired
    private Environment env;

    @Bean
    public ApiInfo createApiInfo() {
        return new ApiInfoBuilder().
                license(env.getRequiredProperty("api.licence.text")).
                title(env.getRequiredProperty("api.title")).
                description(env.getRequiredProperty("api.description")).
                version(env.getRequiredProperty("api.swagger.version")).
                contact(new Contact(env.getRequiredProperty("api.contact.name"),
                        env.getRequiredProperty("api.contact.website"),
                        env.getRequiredProperty("api.contact.email"))).
                build();
    }

    @Bean
    public Docket apiDocket() {
        final ArrayList<SecurityContext> securityContexts = new ArrayList(1);
        securityContexts.add(SecurityContext.builder().build());

        final List<SecurityScheme> schemeList = new ArrayList<>();
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(createApiInfo())
                .securitySchemes(schemeList)
                .securityContexts(securityContexts)
                .pathMapping("/")
                .select()
                .paths(PathSelectors.any())
                .build();

    }


}