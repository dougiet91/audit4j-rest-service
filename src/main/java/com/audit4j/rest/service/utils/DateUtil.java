package com.audit4j.rest.service.utils;

import com.audit4j.rest.service.utils.exceptions.SystemException;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created By Dougie T Muringani : 25/5/2019
 */
public class DateUtil {


    public static Date createRandomDate(String startDate, String endDate) {
        try {
            DateFormat formatter = new SimpleDateFormat("d-MMM-yyyy");
            Date d1 = formatter.parse(startDate);
            Date d2 = formatter.parse(endDate);
            return new Date(ThreadLocalRandom.current().nextLong(d1.getTime(), d2.getTime()));
        } catch (
                ParseException e) {
            throw new SystemException("Failed to generate random date");
        }
    }
}
