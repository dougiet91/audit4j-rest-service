package com.audit4j.rest.service.utils;

import java.util.*;

/**
 * Created By Dougie T Muringani : 8/11/2018
 */
public class ListUtils {
    public static String printList(List objects) {
        if(objects==null){
            return null;
        }

        if(objects.isEmpty()){
            return "[]";
        }

        StringBuilder builder = new StringBuilder();

        builder.append("[\n");
        objects.forEach(object -> {
            builder.append(object.toString()).append(",\n");
        });
        final int lastIndexOfComma = builder.lastIndexOf(",");
        builder.delete(lastIndexOfComma,lastIndexOfComma+1);
        builder.append("]");
        return builder.toString();
    }
}
