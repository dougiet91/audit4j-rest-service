package com.audit4j.rest.service.utils.exceptions;

import java.io.PrintWriter;
import java.io.StringWriter;

public abstract class ExceptionUtil {

	private ExceptionUtil() {

	}

	public static String generateStackTrace(Exception exception) {
		final StringWriter stringWriter = new StringWriter(500);
		exception.printStackTrace(new PrintWriter(stringWriter));
		return stringWriter.toString();
	}
}
