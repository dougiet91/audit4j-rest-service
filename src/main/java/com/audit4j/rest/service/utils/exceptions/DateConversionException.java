package com.audit4j.rest.service.utils.exceptions;

public class DateConversionException extends RuntimeException{
    public DateConversionException(String message) {
        super(message);
    }
}
