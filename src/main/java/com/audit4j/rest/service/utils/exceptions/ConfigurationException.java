package com.audit4j.rest.service.utils.exceptions;

public class ConfigurationException extends SystemException{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ConfigurationException(String message) {
		super(message);
	}

	public ConfigurationException(String message, Throwable cause) {
		super(message, cause);
	}


}
