package com.audit4j.rest.service.utils;

import com.audit4j.rest.service.utils.enums.CrudOperation;
import com.audit4j.rest.service.utils.messages.internal.response.StandardResponse;

/**
 * Created By Dougie T Muringani : 14/12/2018
 */
public class ResponseUtils {
    public static void setMainSuccess(StandardResponse response, CrudOperation operation) {
        response.setSuccess(true);
        response.setResponseCode("200");
        response.setNarrative(setMainSuccessNarrative(operation));
    }

    private static String setMainSuccessNarrative(CrudOperation operation) {
        String action;
        switch (operation) {
            case CREATE:
                action = "CREATED";
                break;
            case READ:
                action = "READ";
                break;
            case UPDATE:
                action = "UPDATED";
                break;
            case DELETE:
                action = "DELETED";
                break;
            default:
                return "Success";
        }
        return "Record(s) " + action + " Successfully.";
    }
}
