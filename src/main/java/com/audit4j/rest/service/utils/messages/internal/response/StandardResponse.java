package com.audit4j.rest.service.utils.messages.internal.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class StandardResponse {
    @ApiModelProperty(notes = "Success", example = "true", required = true)
    @JsonProperty("success")
    private boolean success;

    @ApiModelProperty(notes = "Narrative", example = "Request Successful", required = true)
    @JsonProperty("narrative")
    private String narrative;

    @ApiModelProperty(notes = "HTTP Response Code", example = "200", required = true)
    @JsonProperty("responseCode")
    private String responseCode;

    @Override
    public String toString() {
        return "StandardResponse{" +
                "                             success=" + success +
                ",\n                             narrative='" + narrative + '\'' +
                ",\n                             responseCode='" + responseCode + '\'' +
                '}';
    }

    public String standardToString(){
        return toString()+"\n";
    }

    public void mapStandardResponse(StandardResponse standardResponse){
        this.success=standardResponse.isSuccess();
        this.narrative=standardResponse.getNarrative();
        this.responseCode=standardResponse.getResponseCode();
    }
}
