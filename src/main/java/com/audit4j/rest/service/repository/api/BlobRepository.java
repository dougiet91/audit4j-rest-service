package com.audit4j.rest.service.repository.api;
import com.audit4j.rest.service.domain.BlobObject;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface BlobRepository extends CrudRepository<BlobObject, Long>
        , JpaSpecificationExecutor<BlobObject> {
    BlobObject save(BlobObject blobObject);

    List<BlobObject> findAll();

    List<BlobObject> findBlobById(@Param("id") String id);
}
