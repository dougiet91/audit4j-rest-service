package com.audit4j.rest.service.repository.config;

import com.audit4j.rest.service.business.config.audit4j.Audit4JConfig;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories({"com.audit4j.rest.service.repository","com.audit4j.rest.service.business.config.audit4j"})
@EntityScan(basePackages = {"com.audit4j.rest.service.domain"})
public class DataRepositoryConfig {
}
