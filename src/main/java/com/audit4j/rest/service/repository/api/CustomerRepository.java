package com.audit4j.rest.service.repository.api;
import com.audit4j.rest.service.domain.Customer;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface CustomerRepository extends CrudRepository<Customer, Long>
        , JpaSpecificationExecutor<Customer> {
    Customer save(Customer customer);

    Customer findCustomerById(Long id);
    List<Customer> findCustomerByFirstName(String name);
    List<Customer> findCustomerByLastName(String name);

    List<Customer> findAll();
}

