package com.audit4j.rest.service.business.config;

import com.audit4j.rest.service.business.services.ObjectGeneratorUtil;
import com.audit4j.rest.service.business.services.auditables.customer.api.CustomerAuditableService;
import com.audit4j.rest.service.business.services.logic.api.CustomerService;
import com.audit4j.rest.service.business.services.logic.impl.CustomerServiceImpl;
import com.audit4j.rest.service.repository.api.BlobRepository;
import com.audit4j.rest.service.utils.config.UtilsConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created By Dougie T Muringani : 14/12/2018
 */
@Configuration
@Import({AuditablesConfig.class, UtilsConfig.class})
public class BusinessConfig {
    @Bean
    CustomerService customerService(CustomerAuditableService customerAuditableService){
        return new CustomerServiceImpl(customerAuditableService);
    }

    @Bean
    ObjectGeneratorUtil objectGeneratorUtil(BlobRepository blobRepository){
        return new ObjectGeneratorUtil(blobRepository);
    }
}
