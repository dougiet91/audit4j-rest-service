package com.audit4j.rest.service.business.services.auditables.customer.impl;

import com.audit4j.rest.service.business.services.auditables.customer.api.CustomerAuditableService;
import com.audit4j.rest.service.domain.Customer;
import com.audit4j.rest.service.repository.api.CustomerRepository;
import com.audit4j.rest.service.utils.enums.EntityStatus;
import com.audit4j.rest.service.utils.exceptions.SystemException;
import org.audit4j.core.annotation.Audit;
import org.audit4j.core.annotation.AuditField;
import org.audit4j.core.annotation.IgnoreAudit;

import java.util.List;
import java.util.Locale;

public class CustomerAuditableServiceImpl implements CustomerAuditableService {

    private CustomerRepository customerRepository;
    
    public CustomerAuditableServiceImpl(CustomerRepository customerRepository){
        this.customerRepository = customerRepository;
    }

    @Override
    @Audit(action = "Save Payment")
    public Customer save(@IgnoreAudit final Customer customer,
                         final Locale locale,
                         @AuditField(field = "username") final String username) {
        return customerRepository.save(customer);
    }

    @Override
    public Customer removeCustomer(final Long id, final Locale locale, final String username) {
        final Customer foundCustomer = findCustomerById(id, locale);
        if (foundCustomer == null) {
            throw new SystemException("Record Delete Failed. Record Does Not exist");
        }
        foundCustomer.setStatus(EntityStatus.DELETED);
        return customerRepository.save(foundCustomer);
    }

    @Override
    public Customer findCustomerById(final Long id, final Locale locale) {
        return customerRepository.findById(id).orElse(null);
    }

    @Override
    public List<Customer> findAllCustomers(final Locale locale) {
        return customerRepository.findAll();
    }

    @Override
    public List<Customer> findCustomerByFirstName(String firstName, Locale locale) {
        return customerRepository.findCustomerByFirstName(firstName);
    }
}
