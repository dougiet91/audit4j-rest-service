package com.audit4j.rest.service.business.services.auditables.customer.api;

import com.audit4j.rest.service.domain.Customer;

import java.util.List;
import java.util.Locale;

/**
 * Created By Dougie T Muringani : 24/5/2019
 */
public interface CustomerAuditableService {
    Customer save(Customer customer, Locale locale, String username);

    Customer removeCustomer(Long id, Locale locale, String username);

    Customer findCustomerById(Long id, Locale locale);

    List<Customer> findAllCustomers(Locale locale);

    List<Customer> findCustomerByFirstName(String firstName, Locale locale);
}
