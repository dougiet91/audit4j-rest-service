package com.audit4j.rest.service.business.config.audit4j;

import lombok.extern.slf4j.Slf4j;
import org.audit4j.core.MetaData;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * Created By Dougie T Muringani : 25/5/2019
 *
*/
//for annotations
    @Slf4j
public class MyMetaData implements MetaData {


    public String getActor() {
       Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof UserDetails) {
            return ((UserDetails) authentication.getPrincipal()).getUsername();
        }
        return "anonymous";
    }

    public String getOrigin() {
        try {
          return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
        }catch(Exception e){
            log.error("Could not detect request origin...");
        }
        return "unidentified";
    }

}
