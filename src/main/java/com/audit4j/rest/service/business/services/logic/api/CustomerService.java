package com.audit4j.rest.service.business.services.logic.api;

import com.audit4j.rest.service.domain.Customer;
import com.audit4j.rest.service.utils.messages.internal.response.CustomerResponse;

import java.util.Locale;

/**
 * Created By Dougie T Muringani : 14/12/2018
 */
public interface CustomerService {

    CustomerResponse save(Customer customer, Locale locale, String username);

    CustomerResponse remove(Long id, Locale locale, String username);

    CustomerResponse findCustomerById(Long id, Locale locale);

    CustomerResponse findCustomerByFirstName(String name, Locale locale);

    CustomerResponse findAll(Locale locale);

}
