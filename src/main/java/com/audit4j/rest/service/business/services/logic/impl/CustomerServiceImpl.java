package com.audit4j.rest.service.business.services.logic.impl;

import com.audit4j.rest.service.business.services.auditables.customer.api.CustomerAuditableService;
import com.audit4j.rest.service.business.services.logic.api.CustomerService;
import com.audit4j.rest.service.domain.Customer;
import com.audit4j.rest.service.utils.enums.CrudOperation;
import com.audit4j.rest.service.utils.exceptions.SystemException;
import com.audit4j.rest.service.utils.messages.internal.response.CustomerResponse;

import java.util.List;
import java.util.Locale;

import static com.audit4j.rest.service.utils.ResponseUtils.setMainSuccess;

/**
 * Created By Dougie T Muringani : 24/5/2019
 */
public class CustomerServiceImpl implements CustomerService {

    private CustomerAuditableService customerAuditableService;

    public CustomerServiceImpl(CustomerAuditableService customerAuditableService) {
        this.customerAuditableService = customerAuditableService;
    }

    @Override
    public CustomerResponse save(Customer customer, Locale locale, String username) {
        CustomerResponse response = new CustomerResponse();
        response.setCustomer(customerAuditableService.save(customer, locale, username));
        setMainSuccess(response,CrudOperation.CREATE);
        return response;
    }

    @Override
    public CustomerResponse remove(Long id, Locale locale, String username) {
        CustomerResponse response = new CustomerResponse();
        final Customer customer;
        try {
            customer = customerAuditableService.removeCustomer(id, locale, username);
            response.setCustomer(customer);
        } catch (SystemException e) {
            response.setNarrative(e.getMessage());
        }
        setMainSuccess(response, CrudOperation.DELETE);
        return response;
    }

    @Override
    public CustomerResponse findCustomerById(Long id, Locale locale) {
        CustomerResponse response = new CustomerResponse();
        final Customer customer;
        try {
            customer = customerAuditableService.findCustomerById(id, locale);
            response.setCustomer(customer);
        } catch (SystemException e) {
            response.setNarrative(e.getMessage());
        }
        setMainSuccess(response, CrudOperation.READ);
        return response;
    }

    @Override
    public CustomerResponse findCustomerByFirstName(String firstName, Locale locale) {
        CustomerResponse response = new CustomerResponse();
        response.setCustomerList(customerAuditableService.findCustomerByFirstName(firstName, locale));
        setMainSuccess(response,CrudOperation.READ);
        return response;
    }

    @Override
    public CustomerResponse findAll(Locale locale) {
        CustomerResponse response = new CustomerResponse();
        final List<Customer> allCustomers = customerAuditableService.findAllCustomers(locale);
        response.setCustomerList(allCustomers);
        setMainSuccess(response,CrudOperation.READ);
        return response;
    }
}
