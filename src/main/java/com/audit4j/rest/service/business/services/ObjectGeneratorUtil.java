package com.audit4j.rest.service.business.services;

import com.audit4j.rest.service.domain.BlobObject;
import com.audit4j.rest.service.repository.api.BlobRepository;
import com.audit4j.rest.service.utils.DateUtil;
import com.audit4j.rest.service.utils.exceptions.SystemException;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


/**
 * Created By Dougie T Muringani : 18/2/2019
 */
@Slf4j
public class ObjectGeneratorUtil {

    private static BlobRepository blobRepository;

    private static final int maxNumberOfRecursions = 50;
    private static int recursions = 0;

    public ObjectGeneratorUtil(BlobRepository blobRepository) {
        this.blobRepository = blobRepository;
    }

    public static BlobObject generateRandomBlob() {
//        doSomethingIntoTheDb();

        Integer randomNum = ThreadLocalRandom.current().nextInt(1, 999 + 1);
        String id = randomNum.toString();
        final List<BlobObject> blobs = blobRepository.findBlobById(id);
        if (blobs == null || blobs.isEmpty()) {
            if (recursions < maxNumberOfRecursions) {
                recursions++;
                return generateRandomBlob();
            }
            else {
                String message = "Failed to generate random object";
                log.error(message);
                throw new SystemException(message);
            }
        }
        return blobs.get(0);
    }

    public static void doSomethingIntoTheDb(){
        blobRepository.findAll().forEach(blobObject -> {
            blobObject.setBlobDate(DateUtil.createRandomDate("01-Jan-1980","31-Dec-2000"));
            log.info("Generated Date:: {}", blobObject.getBlobDate());
            blobRepository.save(blobObject);
        });
        String test = "01/Jan/1980";
        test.replaceAll("/", "-");
    }
}
