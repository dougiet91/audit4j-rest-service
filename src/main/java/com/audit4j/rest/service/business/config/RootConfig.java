package com.audit4j.rest.service.business.config;

/**
 * Created By Dougie T Muringani : 16/1/2019
 */

import com.audit4j.rest.service.business.config.audit4j.Audit4JConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({BusinessConfig.class, Audit4JConfig.class})
public class RootConfig {
}
