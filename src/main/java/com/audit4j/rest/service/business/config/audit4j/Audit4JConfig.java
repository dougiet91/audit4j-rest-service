package com.audit4j.rest.service.business.config.audit4j;

import org.audit4j.core.handler.ConsoleAuditHandler;
import org.audit4j.core.handler.Handler;
import org.audit4j.core.handler.file.FileAuditHandler;
import org.audit4j.core.layout.Layout;
import org.audit4j.core.layout.SimpleLayout;
import org.audit4j.handler.db.DatabaseAuditHandler;
import org.audit4j.integration.spring.AuditAspect;
import org.audit4j.integration.spring.SpringAudit4jConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created By Dougie T Muringani : 25/5/2019
 */

@Configuration
@EnableAspectJAutoProxy
public class Audit4JConfig {
    @Autowired
    Environment environment;

    @Bean
    public Layout layout() {
        return new SimpleLayout();
    }

    @Bean
    public MyMetaData metaData() {
        return new MyMetaData();
    }

    @Bean
    public FileAuditHandler fileAuditHandler() {
        return new FileAuditHandler();
    }

    @Bean
    public ConsoleAuditHandler consoleAuditHandler() {
        return new ConsoleAuditHandler();
    }

    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(environment.getProperty("spring.datasource.driver-class-name"));
        dataSourceBuilder.url(environment.getProperty("spring.datasource.url"));
        dataSourceBuilder.username(environment.getProperty("spring.datasource.username"));
        dataSourceBuilder.password(environment.getProperty("spring.datasource.password"));
        return dataSourceBuilder.build();
    }

    @Bean
    public DatabaseAuditHandler databaseHandler() {
        DatabaseAuditHandler databaseHandler = new DatabaseAuditHandler();
        databaseHandler.setDataSource(getDataSource());
        databaseHandler.setEmbedded(environment.getProperty("audit.handlers.database.embedded"));
        databaseHandler.setDb_connection_type(environment.getProperty("audit.handlers.database.connection.type"));
        return databaseHandler;
    }


    @Bean
    public SpringAudit4jConfig auditConfig() {

        List<Handler> handlers = new ArrayList<>();
        handlers.add(databaseHandler());
        handlers.add(consoleAuditHandler());
        handlers.add(fileAuditHandler());

        Map<String, String> props = new HashMap<>();
        props.put("log.file.location", environment.getProperty("audit.handlers.file.location"));

        SpringAudit4jConfig audit4jConfig = new SpringAudit4jConfig();
        audit4jConfig.setHandlers(handlers);
        audit4jConfig.setLayout(layout());
        audit4jConfig.setMetaData(metaData());
        audit4jConfig.setProperties(props);

        return audit4jConfig;
    }

    @Bean
    public AuditAspect auditAspect() {
        return new AuditAspect();
    }
}
