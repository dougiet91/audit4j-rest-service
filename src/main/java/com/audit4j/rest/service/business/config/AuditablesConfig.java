package com.audit4j.rest.service.business.config;

import com.audit4j.rest.service.business.services.auditables.customer.api.CustomerAuditableService;
import com.audit4j.rest.service.business.services.auditables.customer.impl.CustomerAuditableServiceImpl;
import com.audit4j.rest.service.repository.api.CustomerRepository;
import com.audit4j.rest.service.repository.config.DataRepositoryConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Created By Dougie T Muringani : 14/12/2018
 */
@Configuration
@Import({DataRepositoryConfig.class})
public class AuditablesConfig {
    @Bean
    CustomerAuditableService customerAuditableService(CustomerRepository customerRepository){
        return new CustomerAuditableServiceImpl(customerRepository);
    }
}
